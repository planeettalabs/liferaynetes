# liferaynetes

    kubectl create ns <namespace>
    bin/deploy <namespace>

 - Liferay logs a lot of error messages "org.apache.catalina.connector.ClientAbortException: java.io.IOException: Broken pipe" during startup from readinessprobe - tries to mitigate by delaying readinessprobe to 55 seconds...
 - based on https://www.lodatoluciano.com/en/blog/liferay-high-availability


login: test@liferay.com/test


resources:

  - liferay environment variables https://docs.liferay.com/ce/portal/7.1-latest/propertiesdoc/portal.properties.html
  - what does "sticky" mean in memcached/redis sessions: https://github.com/magro/memcached-session-manager/issues/337
  - hikari database pool docs: https://github.com/brettwooldridge/HikariCP#configuration-knobs-baby


## TODO
 - elasticsearch bomb proof (helm chart is shit or not?)
 - disable /app/logs/* somehow, but keep stdout

## Elasticsearch notes

Enable slowlog to log everything - not really useful since just a massive dump

    curl -H'Content-Type: application/json' -XPUT 'http://localhost:9200/_all/_settings?preserve_existing=true' -d '{
      "index.search.slowlog.threshold.query.debug" : "0ms"
    }'

############################################
https://community.liferay.com/forums/-/message_boards/message/4746849
I'm not sure if this is related to your issue, but Liferay does automatically invalidates the current session when a user logs into the portal. Take a look at the property session.enable.phishing.protection for more info.
##########################################

## clustering:
 - https://www.lodatoluciano.com/en/blog/liferay-high-availability
 - https://community.liferay.com/forums/-/message_boards/message/13147457


java -XX:+PrintFlagsFinal -version | grep -Ei "maxheapsize|maxram"

- https://issues.liferay.com/browse/LPS-89569

## NOTES
- PDF processing creates another java process
- fonts for CAPTCHA
  https://github.com/docker-library/openjdk/blob/master/11/jdk/oracle/Dockerfile#L8
  --> libfreetype6 ubuntu package provides libfreetype.so.6
  https://bugs.launchpad.net/ubuntu/+source/openjdk-lts/+bug/1780151
  --> then next error is missing libfontconfig1 that is provided by libfontconfig1

- https://docs.liferay.com/ce/portal/7.1-latest/propertiesdoc/portal.properties.html
- https://github.com/liferay/liferay-portal/blob/master/portal-impl/src/portal.properties

- https://skaffold.dev/docs/references/yaml/


## TODO
 - logs in /app/logs
 - https property
      web.server.protocol=https
 - catalina.out and apache log
 - elastic 6.5
  - automatic backup from ingress
 - duplicate struts path https://issues.liferay.com/browse/LPS-69716
 - /app/osgi
  - state, work, temp

<!-- <Context distributable="true"> -->
 ^--- ??


BEST document to install: https://dev.liferay.com/fr/discover/deployment/-/knowledge_base/7-0/upgrading-to-elasticsearch-6


elastic search indexes from REST curl http://localhost:9200/_cat/indices?v


    kubectl -n exoliferay-de port-forward svc/elasticsearch 9200:9200


    curl http://localhost:9200/_cat/plugins

must return:

    k65XMrg analysis-icu      6.5.4
    k65XMrg analysis-kuromoji 6.5.4
    k65XMrg analysis-smartcn  6.5.4
    k65XMrg analysis-stempel  6.5.4


    curl http://localhost:9200/_cat/indices\?v


delete index

    curl -X DELETE http://localhost:9200/liferay-20099

### load test

with Async sessions: ~80 to be at ~1.0s load
with Sync sessions: ~60 to be at ~1.0s


#---
target_url = "www.exoliferay.de"

tv.stage "visiting" do
  visit target_url

  wait 10 do
    has_text? "iferay"
  end
end

tv.stage "exploring" do
  Superbara.explore2 1,1, /www.exoliferay.de/, random_duration: 1
end
